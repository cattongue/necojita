//
//  AUPutSpoonViewController.h
//  Necojita
//
//  Created by 越智　渉遥 on 2014/10/04.
//  Copyright (c) 2014年 越智　渉遥. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AUNoticeViewController : UIViewController<AVAudioPlayerDelegate>

@end
