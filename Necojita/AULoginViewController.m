//
//  AULoginViewController.m
//  Necojita
//
//  Created by Kuteken Miki on 2014/10/02.
//  Copyright (c) 2014年 ___FULLUSERNAME___. All rights reserved.
//

#import "AULoginViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "Konashi+Grove.h"
#import "AUNekojitaManager.h"
// if you want use nomal mode, switch this.
//#import "Konashi.h"

@interface AULoginViewController ()
@end

@implementation AULoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // nekojita manager initialize
    [AUNekojitaManager sharedManager];
    
    // audio speaker setting
//    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
//    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
//    [audioSession setActive:YES error:nil];
    // audio speaker setting
    AudioSessionInitialize(NULL, NULL, NULL, NULL);
    AudioSessionSetActive(YES);
    UInt32 sessionCategory = kAudioSessionCategory_PlayAndRecord;
    AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof (sessionCategory), &sessionCategory);
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute, sizeof (audioRouteOverride), &audioRouteOverride);
    
    
    // konashi ready settings
    [Konashi initialize];
    [Konashi addObserver:self selector:@selector(ready) name:KONASHI_EVENT_READY];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)find:(id)sender
{
    NSLog(@"find");

    [Konashi find];
}

- (void)ready
{
    NSLog(@"ready");

    UIViewController *controller = [[self storyboard] instantiateViewControllerWithIdentifier:@"SelectUserViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
