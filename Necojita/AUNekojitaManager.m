//
//  AUNekojitaManager.m
//  Necojita
//
//  Created by 越智　渉遥 on 2014/10/04.
//  Copyright (c) 2014年 越智　渉遥. All rights reserved.
//

#import "AUNekojitaManager.h"

@implementation AUNekojitaManager {
    
}

static AUNekojitaManager* sharedManager_ = nil;
+ (AUNekojitaManager *)sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager_ = [AUNekojitaManager new];
    });
    return sharedManager_;
}

@end
