//
//  AUNekojitaManager.h
//  Necojita
//
//  Created by 越智　渉遥 on 2014/10/04.
//  Copyright (c) 2014年 越智　渉遥. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AUNekojitaManager : NSObject

@property (nonatomic) BOOL necojitaStandby;
@property (nonatomic) NSInteger necojitaTemperature;
@property (nonatomic) BOOL necojitaCalled;

+ (AUNekojitaManager *)sharedManager;

@end
