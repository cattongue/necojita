//
//  AUSettingViewController.m
//  Necojita
//
//  Created by 越智　渉遥 on 2014/10/04.
//  Copyright (c) 2014年 越智　渉遥. All rights reserved.
//

#import "AUSettingViewController.h"
#import "Konashi+Grove.h"
#import "AUNekojitaManager.h"

@interface AUSettingViewController ()

@end

@implementation AUSettingViewController {
    
    __weak IBOutlet UIView *putCupView;
    __weak IBOutlet UIView *pushButtonView;
    __weak IBOutlet UILabel *settingValue;
    __weak IBOutlet UILabel *alertText;
    
    NSTimer *standByTimer;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // PIO0 button read settings
    [Konashi digitalRead:PIO0];
    [Konashi addObserver:self selector:@selector(pushButton) name:KONASHI_EVENT_UPDATE_PIO_INPUT];
    [Konashi readGroveAnalogPort:A0];
}

- (void)viewWillAppear:(BOOL)animated
{
    // view settings
    putCupView.hidden = NO;
    pushButtonView.hidden = YES;
    
    standByTimer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(changeView) userInfo:nil repeats:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction

-(IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Konashi Observer

- (void)pushButton
{
    int pio0State = [Konashi digitalRead:(int)PIO0];
    
    if (pio0State) {
        NSLog(@"pushed");
        
        [self setTemperature];
    }
}

#pragma mark - Private

- (void)setTemperature
{
    AUNekojitaManager *manager = [AUNekojitaManager sharedManager];
    if (!manager.necojitaStandby) {
        manager.necojitaTemperature = [Konashi analogRead:AIO0];
        
        settingValue.text = [ NSString stringWithFormat : @"%d", manager.necojitaTemperature];
        NSLog(@"necojitaTemperature:%d", manager.necojitaTemperature);
        
        alertText.text = @"設定されたニャ！";
        
        if ([standByTimer isValid]) {
            [standByTimer invalidate];
            standByTimer = nil;
        }
    }
}

- (void)changeView
{
    putCupView.hidden = YES;
    pushButtonView.hidden = NO;
}

@end
