//
//  AUSelectUserViewController.m
//  Necojita
//
//  Created by 越智　渉遥 on 2014/10/04.
//  Copyright (c) 2014年 越智　渉遥. All rights reserved.
//

#import "AUSelectUserViewController.h"
#import "Konashi+Grove.h"

@interface AUSelectUserViewController ()

@end

@implementation AUSelectUserViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [Konashi pinMode:LED5 mode:OUTPUT];
    [Konashi digitalWrite:LED5 value:HIGH];
    [Konashi pwmMode:LED2 mode:KONASHI_PWM_ENABLE_LED_MODE];
    [Konashi pwmMode:LED3 mode:KONASHI_PWM_ENABLE_LED_MODE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Action

- (IBAction)babyButton:(id)sender
{
    UIViewController *controller = [[self storyboard] instantiateViewControllerWithIdentifier:@"NoticeViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)motherButton:(id)sender
{
}

- (IBAction)settingButton:(id)sender
{
    UIViewController *controller = [[self storyboard] instantiateViewControllerWithIdentifier:@"SettingViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)catButton:(id)sender
{
}

@end
