//
//  AUPutSpoonViewController.m
//  Necojita
//
//  Created by 越智　渉遥 on 2014/10/04.
//  Copyright (c) 2014年 越智　渉遥. All rights reserved.
//

#import "AUNoticeViewController.h"
#import "Konashi+Grove.h"
#import "AUNekojitaManager.h"

@interface AUNoticeViewController ()

@end

@implementation AUNoticeViewController {
    
    __weak IBOutlet UIView *putCupView;
    __weak IBOutlet UIView *noticeView;
    __weak IBOutlet UIButton *standbyModeButton;
    __weak IBOutlet UIButton *backButton;
    __weak IBOutlet UILabel *currentValue;
    
    NSTimer *timer;
    NSTimer *standByTimer;
    AVAudioPlayer *sound;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AUNekojitaManager *manager = [AUNekojitaManager sharedManager];
    if (!manager.necojitaTemperature) {
        standbyModeButton.enabled = NO;
    }
    else {
        standbyModeButton.enabled = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // view settings
    putCupView.hidden = NO;
    noticeView.hidden = YES;
    
    standByTimer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(setStandbyMode) userInfo:nil repeats:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)standbyAction
{
    AUNekojitaManager *manager = [AUNekojitaManager sharedManager];
    manager.necojitaStandby = YES;
}

- (void)resetAction
{
    AUNekojitaManager *manager = [AUNekojitaManager sharedManager];
    manager.necojitaStandby        = NO;
    manager.necojitaCalled         = NO;
    manager.necojitaTemperature    = 0;
    [Konashi pwmMode:LED2 mode:DISABLE];
}

- (void)setStandbyMode
{
    putCupView.hidden = YES;
    noticeView.hidden = NO;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.15f target:self selector:@selector(checkAnalogValue) userInfo:nil repeats:YES];
    [timer fire];

    AUNekojitaManager *manager = [AUNekojitaManager sharedManager];
    if (manager.necojitaStandby) {
        // case of push "stop standby"
        [self resetAction];
    } else {
        // case of push "standby"
        [self standbyAction];
    }
}


#pragma mark - IBAction

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Konashi Observer

- (void)checkAnalogValue
{
    // AIO0 volume read/timer settings
    [Konashi readGroveAnalogPort:A0];

    int aio0Value = [Konashi analogRead:AIO0];
    currentValue.text = [ NSString stringWithFormat : @"%d", aio0Value];
    
    
    AUNekojitaManager *manager = [AUNekojitaManager sharedManager];
    
    NSLog(@"necojitaTemperature: %d\n aio0Value: %d", manager.necojitaTemperature, aio0Value);

    if (manager.necojitaStandby
        && !manager.necojitaCalled
        && aio0Value < manager.necojitaTemperature) {
        // text
        NSLog(@"nya~~~~~");
        
//        AVAudioSession *session = [AVAudioSession sharedInstance];
//        NSArray *arry = session.currentRoute.outputs;
//        AVAudioSessionPortDescription *portDescription = [arry lastObject];
        
        // audio
        NSString *path = [[NSBundle mainBundle] pathForResource:@"nyao" ofType:@"mp3"];
        NSURL *url = [NSURL fileURLWithPath:path];
        sound = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [sound setNumberOfLoops:0];
        [sound play];
        
        // Blink LED2
        [Konashi pwmMode:LED2 mode:KONASHI_PWM_ENABLE_LED_MODE];
        [Konashi pwmPeriod:LED2 period:1000000];
        [Konashi pwmDuty:LED2 duty:1000000];
        [Konashi pwmMode:LED2 mode:ENABLE];
        
        // Blink LED3
        [Konashi pwmMode:LED3 mode:KONASHI_PWM_ENABLE_LED_MODE];
        [Konashi pwmPeriod:LED3 period:1000000];
        [Konashi pwmDuty:LED3 duty:1000000];
        [Konashi pwmMode:LED3 mode:ENABLE];
        
        manager.necojitaCalled = YES;
        
        if ([timer isValid]) {
            [timer invalidate];
            timer = nil;
        }
        
        if ([standByTimer isValid]) {
            [standByTimer invalidate];
            standByTimer = nil;
        }
    }
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    
}

@end
